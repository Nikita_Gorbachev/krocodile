﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Bot : MonoBehaviour
{
    public Transform Player;
    float distanse;
    NavMeshAgent myAgent;
    Animator myAnim;

    void Start()
    {
        myAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        myAnim = GetComponent<Animator>();
    }

    void Update()
    {
        distanse = Vector3.Distance(transform.position, Player.position);
        if (distanse > 100)
        {
            myAgent.enabled = false;
            myAnim.SetBool("model", true);
            myAnim.SetBool("Hodba", false);
            myAnim.SetBool("Bitva", false);
            myAnim.SetBool("Death", false);
        }
        if (distanse <=  100)
        {
            myAgent.enabled = true;
            myAgent.SetDestination(Player.position);
            myAnim.SetBool("model", false);
            myAnim.SetBool("Hodba", true);
            myAnim.SetBool("Bitva", false);
            myAnim.SetBool("Death", false);
        }
        if (distanse <=2.5)
        {
            myAgent.enabled = false;
            myAgent.SetDestination(Player.position);
            myAnim.SetBool("model", false);
            myAnim.SetBool("Hodba", false);
            myAnim.SetBool("Bitva", true);
            myAnim.SetBool("Death", false);
        }
    }
}
