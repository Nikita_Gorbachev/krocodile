﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour
{
    public float Time1;
    public GameObject Kraken;
    public Transform SpawnPoint;

    void Start()
    {
        
    }

    void Update()
    {
        Time1 += Time.deltaTime;
        if (Time1 >= 2)
        {
            Instantiate (Kraken, SpawnPoint.position, Quaternion.identity);
            Time1 = 0;
        }
    }
}
