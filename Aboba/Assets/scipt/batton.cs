﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class batton : MonoBehaviour
{
    public GameObject obj;
    public bool On;
    public GameObject objHand;
    public Transform SpawnPoint;
    public Transform cub;
    public Transform cub2;
    public float Dist;
    public float Dist1;

    // Start is called before the first frame update
    void Start()
    {
        obj.SetActive(true);
        On = true;

        objHand.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        Dist = Vector3.Distance(cub.position, cub2.position);
        Debug.Log("Дистанция" + Dist);

        if (Input.GetKeyDown(KeyCode.E) & On  == true & Dist <= 2)
        {
        
            obj.SetActive(false);
            On = false;

            objHand.SetActive(true);
            
        }

        else if (Input.GetKeyDown(KeyCode.Q) & On == false)
        {
            obj.SetActive(true);
            On = true;

            objHand.SetActive(false);

            Instantiate(obj, SpawnPoint.position, Quaternion.identity);
        }
    }
}
