﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trigger : MonoBehaviour
{
    public GameObject ObgTrig;
    public Transform SpawnPoint;
    public GameObject X12;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            ObgTrig.SetActive(false);
            Debug.Log("Trigger");
            Instantiate(X12, SpawnPoint.position, Quaternion.identity);
        }

    }
    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            ObgTrig.SetActive(true);
        }
    }
} 
