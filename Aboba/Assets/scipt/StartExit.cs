﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class StartExit : MonoBehaviour
{
    public void StartGame()
    {
        SceneManager.LoadScene(1);
        SceneManager.LoadScene("LowPolyDungeonsLite_Demo");
    }

    public void ExitGame()  
    {
        Application.Quit();
        Debug.Log("ExitGame");
    }
}
